<header class="navbar is-dark">
  <div class="navbar-brand">
    <a class="navbar-item">
      <h2>Bender cmd</h2>
    </a>
    <p class="navbar-item">
    {subtitle}
    </p>
  </div>


  <!-- This "nav-toggle" hamburger menu is only visible on mobile -->
  <!-- You need JavaScript to toggle the "is-active" class on "nav-menu" -->
  <span class="navbar-burger burger">
    <span></span>
    <span></span>
    <span></span>
  </span>

  <!-- This "nav-menu" is hidden on mobile -->
  <!-- Add the modifier "is-active" to display it on mobile -->
  <div class="navbar-end navbar-menu">

    <div class="navbar-item">
      <div class="field is-grouped">
        <p class="control">
          <a id="slack" if="{!hasKeys()}" href="https://slack.com/oauth/authorize?scope=chat:write:user&client_id=42536416886.117162730817">
                <img src="https://api.slack.com/img/sign_in_with_slack.png" />
            </a>
        </p>
        <p class="control">
          <button class="button" show={localStorage.token} onclick={logout} class="btn navbar-btn navbar-right btn-default">Logout</button>
        </p>
      </div>
    </div>
  </div>

    <style>
        #slack {
            padding-top: 5px;
        }
        header.nav {
            background-color: black;
        }
    </style>

    <script>
        this.titles = [
            'because I am lazier than Bender.',
            'when all you have is a hammer script - every app looks like a nail',
            'because our deployment tool is error prone.',
            'because ... why I want to type?? why??',
            'one script to deploy them all and in failure bind them'
        ]
        var index = Math.floor(Math.random() * this.titles.length)
        this.subtitle = this.titles[index]

        logout(e) {
            fetch('https://slack.com/api/auth.revoke?token=', {method: 'POST', body: new FormData({token: localStorage.token})})
                .then(function () {
                    delete localStorage.token
                    delete localStorage.code
                    riot.update()
                })
        }

        hasKeys(e) {
            var doHide = !isNotEmpty(localStorage.secret, localStorage.clientId) ||
                (isNotEmpty(localStorage.secret, localStorage.clientId) && typeof localStorage.token == 'string')
            return doHide
        }

    </script>
</header>