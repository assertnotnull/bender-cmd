<projects>
<div class="columns">
    <div class="column is-3">
    <aside class="menu">
        <ul class="menu-list">
            <li onclick="{unsetProject}"><a class="{is-active: !selproject}" href="#">Overview</a>
            </li>
            <li each="{key, name in opts.projects}">
                <a class="{is-active: name == selproject} has-icon-left" data-project="{name}" onclick="{changeProject}">
                    <span class="panel-icon">
                      <i class="fa fa-book"></i>
                    </span>
                    {name}
                    <button class="button" onclick={doActionMenu} data-project={name} if={localStorage.token}>list</button>
                </a>
            </li>
        </ul>
        <div id="side-bottom"><a href="http://riotjs.com" target="_blank">Powered by riot {riot.version}</a></div>
    </aside>
    </div>
    <div class="column is-7">
    <div if={!selproject} class="content">
        <h2>Bender cmd - because it's not automated enough.</h2>
        <h3>For help</h3>
        <p>Type <code>help</code> in chat</p>
        <h3>For your permissions</h3>
        <p>Type <code>show acl</code> in chat</p>
        <h3></h3>
        <h3>deploy help</h3>
        <div class="box">
        <pre id="deploy-help" >List of options on our SysAdmin Wiki:
https://sites.google.com/a/jomediainc.com/sysadmins/saltstack/salt-deploy-framework/how-to-using-deploy_md5-sh
  -h hostname (coma separated list of hostnames or predefined host group) use hostname for targeting
  -a github project name
  -N if you want use predefined groups for targeting
  -G if you want to use app tag/branch for targeting
  -s state (default deploy|file_update|app_update ...)
  -b branch (default latest or master if no tag used)
  -e environment (qa|qa-v{1-9}|stage{1-9}|prod{1}|stage-v{1,9})
  -f force (default False, if you want to redeploy the same project branch)
  -u list/show application users on server (use with -h )
  -l show all servers where app is installed (use with -a)
  -t test (dry run)
  --ddc should be used by ddc guys
Usage examples:
  Get options list:
    help deploy
  Get all server names where app is installed:
    deploy -a drm -l
  Get all [project_name - user] pairs installed on a single host
    deploy -h adserving-us-east-1-nodejs-zombie-109 -u
  Deploy application to a single host, list of hosts or a pre-defined group:
    deploy -h adserving-us-east-1-nodejs-zombie-109 -a adserving_nodejs -b 0.1.5 -e prod -t  (single host)
    deploy -h book-service,book-service-standby -a drm -b 0.1.3 -e prod -t  (deploy to list of hosts)
    deploy -N gtcom -a sqs-daimon -b 0.1.7-rc1 -e prod1 -t (deploy to predefined group of hosts)
  Deploy application to hosts with old version
    deploy -G 2.0.2 -a drm -e stage1 -b 3.0.0 -f -t
    deploy -G master -a drm -e stage1 -b develop -f -t
  Deploy new stage1-7|prod|qa environments:
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b 0.1.7-rc1 -e stage2 -t (deploying the stage2 environment)
    deploy -N gtcom -a sqs-daimon -b 0.1.7-rc1 -e prod1 -t (deploying the prod1 environment)
  Deploy app using tags or branch
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b 0.1.7-rc1 -e stage1 (deploying of project tag 0.1.7-rc1)
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b master -e stage1 (deploy of master branch)
  Deploy just app without configs
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b master -e stage1 -s app_update
  Deploy configs without app
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b master -e stage1 -s file_update
  Test deployment without making any changes (dry run)
    deploy -h staging-adserving-nodejs-zombie -a adserving_nodejs -b master -e stage1 -f -t
        </pre>
        </div>
    </div>

    <div if={selproject} class="content">
        <h2>{selproject}</h2>
        <form onsubmit="{submit}">
            <div class="field is-horizontal">
                <div class="field-label">
                    <label class="label">Environment</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control">
                        <label class="radio" each="{key, env in opts.projects[selproject]}">
                            <input name="project" type="radio" value="{env}" class="radiobox-scatman" checked="{env == selenv}" onclick="{changeEnv}">
                            {env}
                        </label>
                    </div>
                    </div>
                </div>
            </div>
            <div class="field is-horizontal" if={selproject}>
                <div class="field-label">
                    <label class="label">Host</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control is-vertical">
                        <label class="radio" each="{key, host in opts.projects[selproject][selenv]}">
                            <input name="env" type="radio" value="{host}" class="radiobox-wheel" checked="{host == selhost}" onclick="{changeHost}">
                            {host}
                        </label>
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal" if="{selenv}">
                <div class="field-label">
                    <label class="label">Instance</label>
                </div>
                <div class="field-body">
                    <div class="field">
                    <div class="control is-vertical" >
                        <label class="radio" each="{instance in opts.projects[selproject][selenv][selhost]}">
                            <input name="instance" type="radio" value="{instance.name}" class="radiobox-return" checked="{instance.name == selenv}" onclick="{changeInstance}">
                            {instance.name}
                        </label>
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Tag/branch</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control">
                        <input class="input" type="text" name="tag" placeholder="tag or branch..." oninput="{changeTag}" onclick="{selectAll}">
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label">
                    <label class="label">Options</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control" id="options">
                        <label class="radio"><input type="radio" onclick="{setOption}" name="options" checked> <span>none</span></label>
                        <label class="radio"><input type="radio" onclick="{setOption}" name="options" data-option="app_update"> <span>only app</span></label>
                        <label class="radio"><input type="radio" onclick="{setOption}" name="options" data-option="file_update"> <span>only config</span></label>
                        <label class="radio"><input type="radio" onclick="{setOption}" name="options" data-option="pkg_install"> <span>with package</span></label>
                        <label class="radio"><input type="checkbox" onclick={setTest}> <span>test only</span></label>
                        <label class="radio"><input type="checkbox" onclick={setHostless} checked> <span>staging deployment without host?</span></label> <!-- infra changed this in our back -->
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Burp command</label>
                </div>
                <div class="field-body">
                    <div class="field">
                    <div class="control">
                        <input class="input" name="code" id="code" class="col-md-6" value="{buildAction()}">
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Copy?</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control">
                        <button class="button is-light" id="copy" data-clipboard-target="#code">Bite my shiny metal ass! (copy above)</button>
                        <button class="button is-light" data-clipboard-text="deploy -a {selproject} -l">copy `deploy -a {selproject} -l`</button>
                    </div>
                    </div>
                </div>
            </div>

            <div class="field is-horizontal">
                <div class="field-label is-normal">
                    <label class="label">Send to slack</label>
                </div>
                <div class="field-body">
                    <div class="field is-narrow">
                    <div class="control">
                        <button class="button is-primary" onclick={invokeCommand} >Do burp command</button>
                        <button class="button is-light" onclick={invokeList}>Do list project</button>
                    </div>
                    </div>
                </div>
            </div>

        </form>
    </div>
    </div>
</div>




    <style>
        .is-vertical {
            display: flex;
            flex-direction: column;
        }
        .is-vertical > label.radio {
            margin: 3px 0px;
        }
        h2, form {
            padding-top: 1em;
        }
        #deploy-help {
            max-height: 35em;
        }

        #options > .radio:not(:first-of-type) [type="radio"]:checked + span {
          color: red
        }
    </style>

    <script>
        var self = this
        new Clipboard('.copy')
        this.instances = []
        self.bot = typeof self.bot != 'undefined' ? self.bot : 'bender'
        this.selenv = ''

        doAction = function(action) {
            vex.dialog.confirm({
                                message: 'Send `' + action + '`?',
                callback: function (value) {
                    if (value) {
                        var formData = new FormData()
                        formData.append('as_user', true)
                        formData.append('channel', '@' + self.bot)
                        formData.append('token', localStorage.token)
                        formData.append('text', action)
                        fetch('https://slack.com/api/chat.postMessage', {
                            method: 'POST',
                            body: formData
                        }).then(function (response) {
                            if (response.status >= 200 && response.status < 300) {
                                return response.json()
                            } else {
                                throw new Error(response.statusText)
                            }
                        }).then(function (json) {
                            if (!json.ok) {
                                alertify.error(json.error)
                            } else {
                                alertify.success("<img src='https://media0.giphy.com/media/STXkR8yaFwYCc/200w.gif'>")
                            }
                        });
                    }
                }
            })
        }

        doActionMenu(e) {
            e.stopPropagation()
            var action = 'deploy -a ' + e.target.dataset.project + ' -l'
            doAction(action)
        }

        invokeList(e) {
            var action = 'deploy -a ' + this.selproject + ' -l'
            doAction(action)
        }

        invokeCommand(e) {
            if (asEmpty(this.tagname) !== '' && asEmpty(this.selhost) !== '' && this.hasInstance()) {
                doAction(this.buildAction())
            } else {
                alertify.error('Incomplete command')
            }
        }

        buildAction() {
            var env = this.selinstance || this.selenv
            if (this.selenv != 'prod' && !this.hostless) { //infra decided that staging server no longer use host
                var action = 'deploy -a ' + asEmpty(this.selproject) + ' -b ' +
                    asEmpty(this.tagname) + ' -e ' + asEmpty(env) + ' -f ' + asEmpty(this.option) + asEmpty(this.isTest)
            } else {
                var action = 'deploy -a ' + asEmpty(this.selproject) + ' -h ' + asEmpty(this.selhost) + ' -b ' +
                    asEmpty(this.tagname) + ' -e ' + asEmpty(env) + ' -f ' + asEmpty(this.option) + asEmpty(this.isTest)
            }
            return action;
        }

        asEmpty = function(str) {
            return typeof str == 'undefined' || str == null ? '' : str
        }

        changeBot(e) {
            this.bot = e.target.dataset.bot
            console.log(this.bot);
        }

        setTest(e) {
            this.isTest = e.target.checked ? " -t" : null
            this.update()
        }

        setHostless(e) {
            this.hostless = !this.hostless
        }

        hasInstance() {
            return this.instances.length == undefined || this.instances.length > 0 && this.instances.find(function (element) {
                    return element.name == this.selinstance
                }.bind(this))
        }

        unsetProject(e) {
            this.selproject = ''
        }

        changeProject(e) {
            this.selproject = e.target.dataset.project
            this.selenv = ''
            this.reset()
        }

        changeEnv(e) {
            this.selenv = e.target.value
            this.reset()
        }

        changeInstance(e) {
            this.selinstance = e.target.value
            if (this.selinstance.url) {
                this.url = this.selinstance.url
            }
        }

        changeHost(e) {
            this.selhost = e.target.value
            this.instances = opts.projects[this.selproject][this.selenv][this.selhost]
        }

        changeTag(e) {
            e.target.value = e.target.value.trim()
            this.tagname = e.target.value
            return true
        }

        setOption(e) {
            this.option = e.target.dataset.option ? "-s " + e.target.dataset.option : undefined
        }

        selectAll(e) {
            e.target.select();
        }

        submit(e) {
            e.preventDefault()
            return false //never submit
        }

        reset() {
            this.selinstance = null
            this.selhost = null
            this.instances = []
        }

    </script>
</projects>
